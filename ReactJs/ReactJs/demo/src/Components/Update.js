import axios from 'axios';
import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useState, useEffect, useRef } from 'react';

const Edit = () => {

  const navigate = useNavigate();
  const {empId} = useParams();

  const [employee, setEmployee] = useState({
    empId:"",
    empName:"",
    age:"",
    Gender:""
  });
  

  useEffect(()=> {
    axios.get("http://localhost:6100/getempbyid/"+empId).then(data =>{
    setEmployee(data.data);
    }
    )},[]);

  
 const update =()=>{
    axios.put('http://localhost:6100/update',employee).then(data =>{
        console.log("updated successfully.."+data.data)})
    navigate('/fetch')
  }
 const handleUserData=(e)=>{
  const name = e.target.name;
  const value = e.target.value;
  
  setEmployee({
    ...employee, [name] : value
  })
}
  return (
    <div>
        <form>
        <input type="text" name="empId" placeholder="employee id" onChange={handleUserData} value={employee.empId} /><br/>
        <input type="text" name="empName" placeholder="employee name" onChange={handleUserData} value={employee.empName}/><br/>
        <input type="text" name="salary" placeholder="salary" onChange={handleUserData} value={employee.salary}/><br/>
        <input type="text" name="gender" placeholder="gender" onChange={handleUserData} value={employee.gender}/><br/>
        <input type="text" name="city" placeholder="city" onChange={handleUserData} value={employee.city}/><br/>
        <input type="text" name="emailId" placeholder="email id" onChange={handleUserData} value={employee.emailId}/><br/>
        <input type="password" name="password" placeholder="password" onChange={handleUserData} value={employee.password}/><br/>
        <button type="submit" className="btn btn-primary" onClick={update}>Update Here</button>
        </form>
   </div>
  )
}

export default Edit
