import React from 'react'

export default function Click() {
    function clickMe(){
        return <h1>Button Clicked...</h1>
    }
  return (
    <div>
      <button onClick={clickMe}>Click</button>
      <h1>Click the Button</h1>
    </div>
  )
}
