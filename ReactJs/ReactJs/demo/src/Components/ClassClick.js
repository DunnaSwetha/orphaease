import React, { Component } from 'react'

 class ClassClick extends Component {
  
  render() {
 
    return (
      <div>
        <button onClick={this.ClickMe}>Click Me</button>
      </div>
    )
  }
  ClickMe(){
    console.log("button Clicked")
}
}

export default ClassClick
