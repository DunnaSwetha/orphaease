import React, { useState, useEffect } from 'react';

const ImageLoader = () => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const img = new Image();
    img.src = 'https://media.tenor.com/_oPX6F1QqkMAAAAC/hand-anime.gif'; // Replace with the path to your preloader image

    img.onload = () => {
      // Once the preloader image is loaded, set the loaded state to true
      setLoaded(true);
    };
  }, []);

  return (
    <div className={`preloader ${loaded ? 'preloader-hidden' : ''}`}>
      <img src="path-to-your-preloader-image.jpg" alt="Preloader" />
    </div>
  );
};

export default ImageLoader;
