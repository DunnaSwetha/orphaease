
import React, {useEffect, useState} from 'react'
import axios from 'axios'


const Fetch = () => {
  const [employees, setEmployees] = useState([]);
  const [delState, setDelState] = useState(false);
  
  useEffect (() => {
    axios.get("http://localhost:6100/fetch").then(data => {
        setEmployees(data.data)
      }
    )
  },[delState]);


    const deleteEmployee = (empId) =>{
      axios.delete(`http://localhost:6100/delete/${empId}`)
      .then(res =>{setDelState(!delState)})
    }
      
  return (
    <div>
     
     <h1>Details of Employees</h1>
            <table className="table table-bordered table-hover table-striped">
                <thead>
                    <tr><th>Id</th><th>Name</th><th>Age</th><th></th>Gender<th>Actions</th></tr>
                </thead>
                <tbody>
                    {employees.map((employee) => (
                        <tr>
                            <td>{employee.empId}</td>
                            <td>{employee.empName}</td>
                            <td>{employee.age}</td>
                            <td>{employee.Gender}</td>  
                           
            
                           
                            <td><button onClick={()=>deleteEmployee(employee.empId)}>Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
      
    </div>
  )
}

export default Fetch
