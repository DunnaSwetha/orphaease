import React, { Component } from 'react'

export default class EventBind extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
        message:"This is Vishnu"
         
      }
      this.Name = this.Name.bind(this)
    }

    Name(){
        this.setState({
            message:"This is Daedeep"
        })
    }
    
  render() {
    return (
      <div>
        <h1>{this.state.message}</h1>
        {/* <button onClick={this.Name.bind(this)}>Click</button> */}
        {/* <button onClick={() => {this.Name()}}>Click</button> */}
        <button onClick={this.Name}>Click</button>
      </div>
    )
  }
}
