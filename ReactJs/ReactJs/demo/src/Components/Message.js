import React, { Component } from 'react'

export class Message extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
        message:"Hi there!"
         
      }
      this.Ex = this.Ex.bind(this)

    }

    Ex(){
        this.setState({
            message: "I am Fine..."
        })
    }
    
  render() {
    return (
      <div>
        <h1>{this.state.message}</h1>
        <button onClick={this.Ex}>Click</button>
      </div>
    )
  }
}

export default Message
