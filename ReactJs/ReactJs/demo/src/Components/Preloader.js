import React, { useEffect } from 'react'
import "./Preloader.css"
import { preLoaderAnim } from './Animations'

const Preloader = () => {
    useEffect(()=>{
        preLoaderAnim()

    })
  return (
    <div className='Preloader'>
        <div className='texts-container'></div>
        <span>
           Developer
        </span>
    </div>
    

  )
}

export default Preloader
