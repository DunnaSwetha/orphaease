import { React } from "react";

const Greet = (props) => {
    return (
    <div>
        <h1>
            This is {props.name} and {props.myName}
        </h1>

        {props.children}    
    </div>
    )

}
export default Greet;      