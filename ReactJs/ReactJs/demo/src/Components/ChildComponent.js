import React from 'react'

function ChildComponent(props) {
  return (
    <div>
      <button onClick = { () => props.greeting(" Child")}>Greet Parent</button>
    </div>
  )
}

export default ChildComponent
