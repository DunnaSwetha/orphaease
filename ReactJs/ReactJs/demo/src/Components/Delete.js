import axios from 'axios';
import React, { useState, useEffect} from 'react'
import { Table } from 'reactstrap'

const Delete = () => {
    const [employee,setEmployee] = useState([]);
    const [delState,setDelState] = useState(false);

    useEffect(()=>{
        axios.get('http://localhost:6100/Fetch').then((data)=>{
            setEmployee(data.data);
        })
    },[delState])

    const delEmployee = (empId)=>{
        axios.delete(`http://localhost:6100/Delete/${empId}`).then(res => {setDelState(!delState)})
    }

  return (
    <div>
      <Table bordered>
        <tr>
            <th>EmpId</th>
            <th>EmpName</th>
            <th>Age</th>
            <th>Gender</th>
        </tr>
        {employee.map((emp)=>(
            <tr>
                <td>{emp.empId}</td>
                <td>{emp.empName}</td>
                <td>{emp.age}</td>
                <td>{emp.Gender}</td>
                <button onClick={delEmployee(emp.empId)}>Delete</button>
            </tr>
        ))}
      </Table>
    </div>
  )
}


export default Delete
