import React, { Component } from 'react'

class Header extends Component {
  render() {
    return (
      <div>
        <h1>This is Header component</h1>
        <h1>Taught by {this.props.name}</h1>
        <h1>Learning {this.props.subject}</h1>
        <h1>{this.props.children}</h1>
      </div>
    )
  }
}

export default Header
