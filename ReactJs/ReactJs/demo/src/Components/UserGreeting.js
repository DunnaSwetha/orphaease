import React, { Component } from "react";

class UserGreeting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false,
    };
  }

  render() {
    return this.state.loggedIn ? (
      <div>Welcome Daedeep</div>
    ) : (
      <div>Hi There!!!</div>
    );
  }
}

export default UserGreeting;
