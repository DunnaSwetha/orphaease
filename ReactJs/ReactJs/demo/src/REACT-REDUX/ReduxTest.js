import React, { Component } from 'react'
import {connect} from 'react-redux'

export  class ReduxTest extends Component {
  render() {
    return (
      <div>
        <h1>Current Balance : is {this.props.Balance}</h1>
      </div>
    )
  }
}

const recieve= (state) =>{
    return{
        Balance : state.bal
    }
}

const send = (dispatch) => {
    return{
        deposit : () => dispatch({type:"DEPOSIT", value:5000}),
        withdraw : () => dispatch({type:"WITHDRAW",value:2000})
    }


}
export default connect(recieve,send)(ReduxTest)