import axios from 'axios'
import React, { Component } from 'react'
import { Button } from 'reactstrap'
import "./Register.css"
import {Link} from "react-router-dom";

  export default class Register extends Component {
  constructor(){
    super()
  }

  render() {
    return (
      
      <div className='auth-form-container'>
      
        <fieldset className='register-form'>
        <legend align="left"><b>Registration Form</b></legend>
        <label><b>Foundation Name</b></label>
        <input type="text" ref="FoundationName" placeholder="Foundation Name" required/><br/>
        <label><b>Foundation Contact</b></label>
        <input type="text" ref="FoundationContact" placeholder="Foundation Contact" required/><br/>
        <label><b>Foundation Type</b></label>
        <input type="text" ref="Type" placeholder="Type" required/><br/>
        <label><b>Foundation Strength</b></label>
        <input type="number" ref="Strength" placeholder="Strength" required/><br/>
        <label><b>Location</b></label>
        <input type="text" ref="Location" placeholder="Location" required/><br/>
        <label><b>Incharge Name</b></label>
        <input type="text" ref="InchargeName" placeholder="Incharge Name" required/><br/>
        <label><b>Phone Number</b></label>
        <input type="text" ref="Phoneno" placeholder="PhoneNo" required/><br/>
        <label><b>Certification</b></label>
        <input type="file" ref="Certification" placeholder="Certificates" required/><br/>
        <label><b>Foundation Photos</b></label>
        <input type="file" ref="Photos" placeholder="File" required/><br/>
        {/* <Button color="primary" onClick={this.register}>Register</Button> */}
        <div align="left">
        <Link to="/" ><Button className="button-28"  color="primary" onClick={this.register}>Register</Button></Link>
        </div>
        </fieldset>
      </div>
    )
  }
  
  register =()=>{
   const Register = {
    FoundationName: this.refs.FoundationName.value,
    FoundationContact: this.refs.FoundationContact.value,
    Type: this.refs.Type.value,
    Strength: this.refs.Strength.value,
    Location : this.refs.Location.value,
    InchargeName: this.refs.InchargeName.value,
    Phoneno: this.refs.Phoneno.value,
    Certification: this.refs.Certification.value,
    Photos:this.refs.Photos.value

    }

    axios.post('http://localhost:4000/Register',Register)
    .then(data=>{
      console.log("success");


    })
}
 
}