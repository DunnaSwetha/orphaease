import axios from 'axios'
import React, { Component } from 'react'
import { Button } from 'reactstrap'
import {Link} from "react-router-dom";
import "./Donor.css";

  export default class Donor extends Component {
  constructor(){
    super()
  }

  render() {
    return (
      <div className='auth-form-container'>
        <fieldset className='register-form'>
        <legend align="left"><b>Donor Registration Form</b></legend>
        <label> <b>Name</b></label>
        <input type="text" ref="Name" placeholder="Name" required/><br/>
        <label><b>Gender</b></label>
        <input type="text" ref="Gender" placeholder="Gender" required/><br/>
        <label><b>Email Id</b></label>
        <input type="email" ref="email" placeholder="email id " required/><br/>
        <label><b>Phone Number</b></label>
        <input type="text" ref="Phoneno" placeholder="PhoneNo" required/><br/>
        <label> <b>DOB</b></label>
        <input type="date" ref="dob" placeholder="date of birth" required/><br/>
        <label> <b>Password</b></label>
        <input type="Password" ref="Password" placeholder="********" required/><br/>
        <div align="left">
       <Link to="/" ><Button  className="button-28" color="primary" onClick={this.donor}>Submit</Button></Link>
       </div>
        </fieldset>
      </div>
    )
  }
  
  donor =()=>{
   const Donor = {
    Name:this.refs.Name.value,
    Gender:this.refs.Gender.value,
    email:this.refs.email.value,
    Phoneno:this.refs.Phoneno.value,
    dob:this.refs.dob.value,
    Password:this.refs.Password.value
   
   
    }

    axios.post('http://localhost:4000/Donor',Donor)
    .then(data=>{
      console.log("success");


    })
}
 
}