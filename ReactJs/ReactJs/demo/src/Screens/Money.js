import './Money.css';
import axios from 'axios';
import React, { Component } from 'react';

export default class Money extends Component {
  constructor() {
    super();
    this.state = {
      fullName: "",
      phoneNumber: "",
      selectedCurrency: "INR",
      amount: 0,
      foundationName: "",
      selectedDonationType: "Wallet",
      upiDetails: "",
      walletDetails: "",
      cardDetails: "",
    };
  }

  handleInputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleDonationTypeChange = (e) => {
    this.setState({
      selectedDonationType: e.target.value,
      upiDetails: "", 
      walletDetails: "",
      cardDetails: "",
    });
  }

  handleFoodDonation = async () => {
    const Money = {
      title: this.state.selectedTitle,
      fullName: this.state.fullName,
      phoneNumber: this.state.phoneNumber,
      currency: this.state.selectedCurrency,
      amount: this.state.amount,
      foundationName: this.state.foundationName,
      donationType: this.state.selectedDonationType,
      upiDetails: this.state.upiDetails,
      walletDetails: this.state.walletDetails,
      cardDetails: this.state.cardDetails,
    };

    try {
      await axios.post('http://localhost:4000/Money', Money);
      alert("Registration of donation successful");
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div className="Wrapper">
        <div className="Container">
          <div className="Section1"></div>
          <div className="Section2">
            <div className="Form-Container">
              <div className="F1">
                <div className="label">Full Name:</div>
                <div className="input-Wrapper">
                  <input
                    type="text"
                    name="fullName"
                    value={this.state.fullName}
                    onChange={this.handleInputChange} />
                </div>
              </div>
              <div className="F1">
                <div className="label"> Phone Number:</div>
                <div className="input-Wrapper">
                  <select>
                    <option>(+91)</option>
                    <option>(+1)</option>
                  </select>&nbsp;&nbsp;&nbsp;&nbsp;
                  <input name="phoneNumber"
                    value={this.state.phoneNumber}
                    onChange={this.handleInputChange}/>
                </div>
              </div>
              <div className="F1">
                <div className="label">Enter Amount:</div>
                <div className="input-Wrapper">
                  <input
                    type="number"
                    name="amount"
                    value={this.state.amount}
                    onChange={this.handleInputChange}
                  />
                </div>
              </div>
              <div className="F1">
                <div className="label">Foundation Name:</div>
                <div className="input-Wrapper">
                  <input
                    type="text"
                    name="foundationName"
                    value={this.state.foundationName}
                    onChange={this.handleInputChange}
                  />
                </div>
              </div>
              <div className="F1">
                <div className="label">Select Donation Type:</div>
                <div className="input-Wrapper">
                  <select
                    name="selectedDonationType"
                    value={this.state.selectedDonationType}
                    onChange={this.handleDonationTypeChange}
                  >
                    <option value="Wallet">Wallet</option>
                    <option value="UPI">UPI</option>
                    <option value="Cards">Cards</option>
                  </select>
                </div>
              </div>
              {this.state.selectedDonationType === "UPI" && (
                <div className="F1">
                  <div className="label">UPI Details:</div>
                  <div className="input-Wrapper">
                    <input
                      type="text"
                      name="upiDetails"
                      value={this.state.upiDetails}
                      onChange={this.handleInputChange}
                    />
                  </div>
                </div>
              )}
              {this.state.selectedDonationType === "Wallet" && (
                <div className="F1">
                  <div className="label">Wallet Details:</div>
                  <div className="input-Wrapper">
                    <input
                      type="text"
                      name="walletDetails"
                      value={this.state.walletDetails}
                      onChange={this.handleInputChange}
                    />
                  </div>
                </div>
              )}
              {this.state.selectedDonationType === "Cards" && (
                <div className="F1">
                  <div className="label">Card Details:</div>
                  <div className="input-Wrapper">
                    <input
                      type="text"
                      name="cardDetails"
                      value={this.state.cardDetails}
                      onChange={this.handleInputChange}
                    />
                  </div>
                </div>
              )}
              <div className="F1">
                <div className="Button-Wrapper">
                  <button onClick={this.handleFoodDonation}>Donate</button>
                  <button onClick={() => window.location.reload()}>Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
