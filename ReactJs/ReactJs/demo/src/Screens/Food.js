import "./Food.css"
import axios from 'axios'
import React, { Component } from 'react'
export default class Food extends Component {
  constructor() {
    super()
  }
  render() {
    return (
      <div className="Wrapper">
        <div className="Container">
          <div id="Section1"></div>
          <div className="Section2">
            <div className="Form-Container">
              <div className="F1">
                <div className="label">Full Name:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="name"/>
                </div>
              </div>
              <div className="F1">
                <div className="label">Date:</div>
                <div className="input-Wrapper">
                  <input type="date" ref="Date" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Time:</div>
                <div className="input-Wrapper">
                  <input type="Time" ref="Time" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Location:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="Location"/>
                </div>
              </div>
              <div className="F1">
                <div className="label">Fondation:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="Foundation"/>
                </div>
              </div>
              <div className="F1">
                <div className="label">Capacity:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="Capacity" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Select Food:</div>
                <div className="input-Wrapper">
                  <select className="FSelect" ref="Fselect">
                    <option value={"Food"}>Food</option>
                    <option value="frutis">fruits</option>
                    <option value="snacks">snacks</option>
                  </select>
                </div>
              </div>
              <div className="Button-Wrapper">
                <button onClick={this.Food}>Donate</button>
                <button onClick={this.Food}>Cancel</button>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  Food = async () => {
    const Food = {
      name: this.refs.name.value,
      Date: this.refs.Date.value,
      Time: this.refs.Time.value,
      Location: this.refs.Location.value,
      Foundation: this.refs.Foundation.value,
      capacity: this.refs.Capacity.value,
     selectFood: this.refs.Fselect.value,
     

    }

     await axios.post('http://localhost:4000/Food', Food)
      .then(data => {
         alert("Registration of donation successful")
         window.location.reload()
      })
      .catch(error=>{
        console.log(error)
      })
  }
}

