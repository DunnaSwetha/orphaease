import React from "react";
import "./Donation.css";
import { Link } from "react-router-dom";

export default function Donation() {
    return (<div class="donation-container">
    <div class="div1">
        <div >
      <h1 class="heading">Food</h1><br/><br/><br/>
      <p><h6><b><i>"When one gives,two get happy".</i></b></h6></p><br/>
      <p>The donation of food is the most satisfying, not only to the one who receives it but to the one who feeds a hungry person.</p>
      <p>Among all the donations one can make, what better than to give to the poor and needy,but to the one who feeds a hungry person.So lets be intrested in donating something to be ourselves happy and satisfied.</p>
      </div>
      <div >
      <Link to="/Food"><button className="button-28">Donate</button></Link>
      </div>
    </div>
    <div class="div2">
      <div><h1  class="heading">Money</h1><br/><br/><br/>
      <p><h6><b><i>"No one has ever become poor from giving".</i></b></h6></p><br/>
      <p>If money we donate helps one child or can ease the pain of one parent,those funds are really well spent.</p>
       <p>It's not how much we give but how much love we put into giving, So lets have hands together and help another hand.</p>
       </div>
      <Link to="/Money"><button className="button-28">Donate</button></Link>
    </div>
    <div class="div3">
        <div>
      <h1  class="heading">Other</h1><br/><br/><br/>
      <p><h6><b><i>"Giving is not just make donation,it's about making difference".</i></b></h6></p><br/>
      <p>Donating various items to orphans is a compassionate gesture that can improve their quality of life.</p> 
      <p>These donations can include clothing, toys, books, school supplies, and personal hygiene items. Such contributions help address the practical and emotional needs of orphaned children.</p>
      </div>
      <Link to="/Others"><button className="button-28">Donate</button></Link>
    </div>
  </div>)
}