import React, { useState } from 'react';
import { Button, Form, FormGroup, Input, Label,Col } from 'reactstrap';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';

function Login2() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const passwordInputType = showPassword ? 'text' : 'password';

  const validateEmail = () => {
    if (!email) {
      setEmailError('Email is required');
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setEmailError('Email is invalid');
    } else {
      setEmailError('');
    }
  };

  const validatePassword = () => {
    if (!password) {
      setPasswordError('Password is required');
    } else if (password.length < 6) {
      setPasswordError('Password must be at least 6 characters');
    } else {
      setPasswordError('');
    }
  };

 const navigate = useNavigate();

  const handleLogin = async () => {
    try {
      const response = await axios.get(`http://localhost:4000/Login2/${email}/${password}`);
  
      if ( email == response.data.email && password == response.data.Password) {
       navigate("/")
       
      } else {
        console.log('Invalid Credentials');
        alert("Invalid Credentials")
      }
    } catch (error) {
      console.log('Error:', error);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    validateEmail();
    validatePassword();

    if (!emailError && !passwordError) {
      // Perform your form submission logic here.
    }
  };

  return (
    <div className="container">
      <div className="center-content">
        <h2>Log-In</h2>
        <Form className="form" onSubmit={handleLogin}>
          <FormGroup>
            <Label for="exampleEmail">Username</Label>
            <Col sm={15}>
              <Input
                type="email"
                name="email"
                id="exampleEmail"
                placeholder="example@example.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                onBlur={validateEmail}
              />
              <p style={{color:"red"}}>{emailError}</p>
            </Col>
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">Password</Label>
            <Col sm={15}>
              <Input
                type="password"
                name="password"
                id="examplePassword"
                placeholder="**"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                onBlur={validatePassword}
              />
              <p style={{color:"red"}}>{passwordError}</p>
            </Col>
          </FormGroup>
          <FormGroup>
            <a href="/forgot-password">Forgot Password</a> {/* Add a link for "Forgot Password" */}
          </FormGroup>
         <Button className="login-button" onClick={handleLogin} disabled={(emailError || passwordError)?true:false}>Login</Button><br/>
          <Button className="register-button">Register</Button>
        </Form>
      </div>
    </div>
  );
};

export default Login2;