import "./Others.css";
import axios from 'axios'
import React, { Component } from 'react'
export default class Others extends Component {
  constructor() {
    super()
  }
  render() {
    return (
      <div className="Wrapper">
        <div className="Container">
          <div className="Section1"></div>
          <div className="Section2">
            <div className="Form-Container">
              <div className="F1">
                <div className="label">FullName:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="name" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Date:</div>
                <div className="input-Wrapper">
                  <input type="date" ref="Date" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Time:</div>
                <div className="input-Wrapper">
                  <input type="Time" ref="Time" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Location:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="Location" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Foundation:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="Foundation" />
                </div>
              </div>
              <div className="F1">
                <div className="label">Capacity:</div>
                <div className="input-Wrapper">
                  <input type="text" ref="Capacity" />
                </div>
              </div>
              <div className="F1">
                <div className="label">OtherType:</div>
                <div className="input-Wrapper">
                  <select className="FSelect" ref="Fselect">
                    <option value={"cloths"}>cloths</option>
                    <option value="things">things</option>
                    <option value="books">books</option>
                  </select>
                </div>
              </div>
              <div className="Button-Wrapper">
                <button onClick={this.Others}>Donate</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  Others = () => {
    const Others = {
      name: this.refs.name.value,
      Date: this.refs.Date.value,
      Time: this.refs.Time.value,
      Location: this.refs.Location.value,
      Foundation: this.refs.Foundation.value,
      Capacity: this.refs.Capacity.value,
      OtherType: this.refs.Fselect.value,

    }

    axios.post('http://localhost:4000/Others', Others)
      .then(data => {
        alert("Registration of donation successful")
        window.location.reload()
      })
      .catch(error => {
        console.log(error)
      })
  }
};
