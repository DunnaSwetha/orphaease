import { useState } from "react";
import "./Login.css"
import axios from "axios"
import { Link } from "react-router-dom";
import { Button } from "reactstrap";


const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = () => {
    const userCredentials = {
      email: email,
      password: password,
    }
    try {
      const response = axios.post('http://localhost:4000/Login2',userCredentials);
      console.log("1");
 
      if (response.data.message === 'Login successful') { 
        console.log("Login successful");
      } else {
        console.log('Invalid Credentials');
      }
    } catch (error) {
      console.log('Error:', error);
    }
    };
  

    return (
      <div className="container">
        
          <label  className="Mail" >LOGIN</label> <br/>
          <table>
        <thead>
          <div className="email">
            <tr>
              <label htmlFor="email">EMAIL </label> {/* Use 'htmlFor' for labels */}
            </tr>
            <tr>
              <input
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="mail"
              />
            </tr>
          </div>
        </thead>
        <tr>
          <td>
            <label htmlFor="password">Password</label> {/* Use 'htmlFor' for labels */}
          </td>
          <td>
            <input
              type="password" // Use lowercase 'password' here
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Password"
            />
          </td>
        </tr>
        <br />
      </table>
          
          <Button color="primary" onclick={handleLogin} >Login</Button>
        
      </div>
    );
};

export default Login;
