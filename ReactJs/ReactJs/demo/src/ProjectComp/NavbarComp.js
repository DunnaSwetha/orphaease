import React, { Component } from "react";
import { Navbar, NavDropdown, Nav, Dropdown } from "react-bootstrap";
import "./Navbar.css";
import { Link } from "react-router-dom";

export default class NavbarComp extends Component {
  render() {
    return (
      <div>
        <Navbar className="nav-main"  expand="lg">
          <Navbar.Brand className="logo" href="/">
            OrphaEase
          </Navbar.Brand>
          
          {/* <Navbar.Toggle aria-controls="basic-navbar-nav" /> */}
          <Navbar.Collapse id="basic-navbar-nav" className="nav-link-bar">
            <Nav.Link Link to ="/">HOME</Nav.Link>
            <Link to="./Login2">LOGIN</Link>
            <NavDropdown title="REGISTRATION" id="basic-nav-dropdown">
              <Link to="/Donor"><NavDropdown.Item href="#action/3.1">Donor Registration</NavDropdown.Item></Link>
              {/* <NavDropdown.Item href="/Register">Foundation Registration</NavDropdown.Item> */}
              <Link to="/Register"><NavDropdown.Item href="#action/3.1">Foundation Registration</NavDropdown.Item></Link>
              </NavDropdown>
            <Nav.Link href="/Donation">DONATE</Nav.Link>

            
            
        
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
