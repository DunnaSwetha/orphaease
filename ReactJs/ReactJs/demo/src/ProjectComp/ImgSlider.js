import React from 'react'
import SimpleImageSlider from "react-simple-image-slider";

const images = [
    

  { url: "https://images.creativemarket.com/0.1.0/ps/1071528/910/606/m1/fpnw/wm0/img_4133-2-.jpg?1457563592&s=bcedd1424874bb8c5531dd2a70083e99" },
  { url: "https://i.pinimg.com/originals/57/36/bb/5736bbb94df3f1fd1f400563ec4ea277.jpg" },
  { url: "https://images.pexels.com/photos/7977929/pexels-photo-7977929.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" },
  { url: "D:/FSD56/ReactJs/demo/src/Components/Images/foundationReg.jpg" },
];

const ImageSlider = () => {
  return (
    <div>
      <SimpleImageSlider
        width={1426}
        height={604}
        images={images}
        showBullets={true}
        showNavs={true}
      
      />
    </div>
  );
}
export default ImageSlider