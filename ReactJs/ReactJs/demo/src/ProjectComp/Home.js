import React from "react";
import Footer from "./Footer";
import NavbarComp from "./NavbarComp";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import "./Home.css";
import { Container } from "react-bootstrap";
import ImageSlider from "./ImgSlider";

export default function Home() {
  return (
    <div>
      <NavbarComp />
      <div className="Container">
        <ImageSlider />
        <h1 className="heading">GIVING UP HATE FOR LENT &nbsp;</h1>
        <br />
        <div className="para">
          <h3>"OrphaEase: Touching Lives, Building Futures."</h3>
        </div>
      </div>
      <div className="content">
        <h2 align="left"><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
          <i>
            <b>"Empowering Stranded Souls : Your Donation Makes a Difference".</b>
            <br />
          </i>
        </h2>
        <h5 align="left">
          Welcome to OrphaEase, where a small act of generosity can create a<br/>
          lifetime of impact. Our mission is to provide hope and support to<br/>
          orphaned children worldwide. We believe in the power of collective<br/>
          compassion to change lives, and this blog post is dedicated to sharing<br/>
          our journey, the stories of those we've touched, and the immense<br/>
          potential that your donations hold.
         
          <br />
          <br />
          <br />
          
          <h3 >
            <b>The OrphaEase Mission</b>
          </h3>
          <p >At OrphaEase, our core mission is simple: to create brighter futures
          for orphaned children by providing them with essential resources,
          love, and care. We believe that every child deserves a chance to
          thrive, regardless of their circumstances. Through the generosity of
          our supporters, we aim to empower these children with the tools they
          need to succeed.</p>
        </h5>
        <br />
        <br />
        <h5 align="left">
          <h3>
            <b>You Can Contribute </b>
          </h3>
          <p>We invite you to join our mission and be a part of the change. Your
          contributions can make a significant difference: One-Time Donations:
          Make a one-time donation to support our ongoing projects and
          initiatives. Monthly Donations: Consider becoming a monthly donor to
          provide continuous support for these children. Volunteer
          Opportunities: We also welcome volunteers who can contribute their
          time and skills to help us further our mission. Spread the Word: Share
          OrphaEase with your friends and family to raise awareness and
          encourage more people to get involved.
          </p>
        </h5>
        <br />
        <h3 align="left">
          <b>"Bringing Smiles: Support Orphans and Charitable Homes"</b>
        </h3>
        <h5 align="left">
          In a world filled with challenges, there's a beacon of hope that
          shines brightly — the love and compassion that generous souls extend
          to orphans and charitable homes. This blog post is dedicated to the
          incredible work being done by individuals and organizations like you
          to make a difference in the lives of those who need it most.
        </h5>
        
        <br/><br/>
          <h3 align="left"><b>Your Role in Making a Difference</b></h3> 
          <h5 align="left">
           Donations: Your contributions can change lives. Even a
          small donation can go a long way in providing essentials to orphans
          and charitable homes. Volunteer Opportunities: Consider volunteering
          your time and skills to make a hands-on impact. Advocate: Share our
          mission and encourage others to join the cause. Together, we can
          create a ripple of kindness.</h5>
          <br/><br/>
          <h3 align="left"> <b>Transparency and Accountability</b></h3> 
          <h5 align="left"> We are committed to transparency and accountability. Your
          donations are used efficiently, and we provide regular updates on how
          your contributions are making a difference. Thank You We want to
          express our deepest gratitude to everyone who has supported us. Your
          kindness is the driving force behind our mission, and we are immensely
          thankful for your trust and generosity. Conclusion Your support for
          orphans and charitable homes is a shining example of the goodness that
          resides in the human heart. Together, we can continue to bring smiles,
          provide opportunities, and create a better world for those who need it
          most. Join us in our mission to make the world a better place for
          orphans and those living in charitable homes.
          </h5>
  
      </div>
    </div>
  );
}
