import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./ProjectComp/Home";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login2 from './Screens/Login2';
import Register from "./Screens/Register";
import Donor from "./Screens/Donor";
import Food from "./Screens/Food";
import Others from "./Screens/Others";
import Money from "./Screens/Money";
import Donation from "./Screens/Donation";
function App() {
  return (
   
    <div className="App">
  
       <Router>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/Login2" element={<Login2 />} />
          <Route exact path="/Register" element={<Register />} />
          <Route exact path="/Donor" element={<Donor />} />
          <Route exact path="/Food" element={<Food/>} />
          <Route exact path="/Others" element={<Others/>}/>
          <Route exact path="/Money" element={<Money/>}/>
          <Route exact path="/Donation" element={<Donation/>}/>
        </Routes>
      </Router> 
    </div> 
  );
}

export default App;
