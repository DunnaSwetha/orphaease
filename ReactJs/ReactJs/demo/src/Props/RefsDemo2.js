import React, { useRef } from 'react'
import { Button } from 'reactstrap';

function RefsDemo2() {
    const loginId = useRef(null);
    const password = useRef(null);

    const login = () => {
        let loginIdValue = loginId.current.value;
        let passwordValue = password.current.value;

        console.log('Data Recieved :', loginIdValue);
    }
  return (
    <div>
      Enter login Id <input type='text' ref = {loginId} /> <br/>
      Enter password <input type='password' ref = {password} /> <br/>
      <Button color = 'danger' onClick={login}>Login</Button>
    </div>
  )
}

export default RefsDemo2
