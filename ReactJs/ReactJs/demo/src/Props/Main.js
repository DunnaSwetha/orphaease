import React from 'react'
import Header from "./Header"
import Footer from './Footer'

const Main = () => {
  return (
    <div>
      <Header subject = "React" trainer = "Pasha" salary = "80000"/>
      <Footer trainer = "Pasha" message = "Hi!!!!"/>
      <Header subject = "Java" trainer = "Harsha" salary = "80000"/>
      <Footer trainer = "Harsha" message = "Hello!!"/>
      <Header subject = "Corporate" trainer = "Pruthvi" salary = "70000"/>
      <Footer trainer = "Pruthvi" message = "Hi!!!!"/>
    </div>
  )
}

export default Main
