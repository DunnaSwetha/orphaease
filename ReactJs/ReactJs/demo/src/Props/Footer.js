import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div>
        <h1>{this.props.trainer}:{this.props.message}</h1>
      </div>
    )
  }
}
