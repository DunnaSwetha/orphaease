import React, { Component } from 'react'

export default class StateEx2 extends Component {
    constructor(){
        super()
        this.state={
            Name: "Krishna", Age:"40", Salary: "3000000" , marStatus: true
        }
    }
  render() {
    return (
      <div>
          <table border="2">
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Salary</th>
                <th>marStatus</th>
            </tr>
            <tr>
                <td>{this.state.Name}</td>
                <td>{this.state.Age}</td>
                <td>{this.state.Salary}</td>
                <td>{JSON.stringify(this.state.marStatus)}</td>


            </tr>
          </table>
      </div>
    )
  }
}
