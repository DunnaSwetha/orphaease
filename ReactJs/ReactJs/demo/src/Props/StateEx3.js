import React ,{useState} from 'react'

const StateEx3 = () => {
    const [employee,setEmployee] = useState({age: 45,name: 'PASHA',salary : 9999.99,marritalStatus : true
    })

    const changeEmployee=()=>{
        setEmployee({...employee,name: 'PASHA MD'})
    }
    return (
      <div>
        <table border="2" align="center">
            <tr><th>Name</th><th>Age</th><th>Salary</th><th>Marrital Status</th></tr>
            <tr><td>{employee.name}</td>
            <td>{employee.age}</td>
            <td>{employee.salary}</td>
            <td>{JSON.stringify(employee.marritalStatus)}</td>
            </tr>
        </table>
        <button onClick={changeEmployee}>Change Message</button>
      </div>
    )
  }

  export default StateEx3;