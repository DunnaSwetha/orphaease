import React, { Component } from 'react'

export default class RefsDemo1 extends Component {
  render() {
    return (
      <div>
        Enter login Id <input type = 'text' ref="loginId"/><br/>
        Enter password <input type = 'password' ref = 'password' /><br/>
        <button color='primary' onClick={()=>this.login()}>Login</button>
      </div>
    )
  }
  login(){
    let loginId = this.refs.loginId.value;
    let password = this.refs.password.value;

    console.log('Recieved Data successfully',loginId)
  }
}
