import React from 'react'

const Header = ({subject,trainer,salary}) => {
  return (
    <div>
      <h1>Learning {subject}</h1>
      <h1>By {trainer}</h1>
      <h1>salary {salary}</h1>
    </div>
  )
}

export default Header
