import React, { Component } from "react";
import { Table } from 'reactstrap';

export default class Assignment extends Component {
  constructor() {
    super();
    this.state = {
      employees: [
        { 
          empId: 10, 
          empName: "PJ",
           position: "Web Developer",
            salary: 10000 
        },
        {
          empId: 11,
          empName: "Sai Teja",
          position: "Java Developer",
          salary: 90000,
        },
        {
          empId: 12,
          empName: "Antara",
          position: "Full Stack Developer",
          salary: 80000,
        },
        {
          empId: 13,
          empName: "Sravya",
          position: "Back End Coder",
          salary: 70000,
        },
        { 
          empId: 14,
           empName: "Ramya", 
           position: "Psycho",
            salary: 65000
        },
        { 
          empId: 15, 
          empName: "Krishna", 
          position: "Tester", 
          salary: 60000
        },
      ],
    };
  }

  handleClearClick = () => {
    this.setState({ employees: [] });
  }

  handleAddRow = () => {
    const newEmployee = {
      empId: this.state.employees.length + 1,
      empName: "New Employee",
      position: "New Position",
      salary: 0,
    };

    this.setState((prevState) => ({
      employees: [...prevState.employees, newEmployee],
    }));
  }

  handleDeleteRow = (empId) => {
    this.setState((prevState) => ({
      employees: prevState.employees.filter((emp) => emp.empId !== empId),
    }));
  }

  render() {
    return (
      <div>
        <Table bordered style={{ backgroundColor: "bisque" }}>
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Position</th>
              <th>Salary</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.state.employees.map((emp) => (
              <tr key={emp.empId} className="Table-primary">
                <td>{emp.empId}</td>
                <td>{emp.empName}</td>
                <td>{emp.position}</td>
                <td>{emp.salary}</td>
                <td>
                  <button onClick={() => this.handleDeleteRow(emp.empId)}>Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
       <center> 
        <button onClick={this.handleClearClick}>Clear the data</button>
      &nbsp;&nbsp;&nbsp;&nbsp;  <button onClick={this.handleAddRow}>Add a row</button>
        </center>
      </div>
    );
  }
}
