import React, { Component } from 'react'

export default class StateEx1 extends Component {
    constructor () {
        super()
        this.state = {
            message : "Hi this is a state object..."
        }
    }
    changeMessage(){
        this.setState({
            message : "Hi! the message is changed..."           
        })
        console.log(this.state.message);
    }
  render() {
    return (
      <div>
        <h2>{this.state.message}</h2>
        <button onClick = { () => this.changeMessage()}>Change Message</button>
      </div>
    )
  }
}
